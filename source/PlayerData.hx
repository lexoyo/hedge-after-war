typedef PlayerData = {
  x: Float,
  y: Float,
  width: Float,
  height: Float,
  angle: Float,
  speedX: Float,
  speedY: Float,
  mouseX: Float,
  mouseY: Float,
  mouseAngle: Float,
}
