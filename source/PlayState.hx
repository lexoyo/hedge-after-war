package;

import flixel.FlxState;
import flixel.addons.nape.*;
import flixel.*;
import flixel.util.*;
import flixel.group.*;
import flixel.math.*;
import flixel.group.FlxGroup;
import flixel.FlxCamera.FlxCameraFollowStyle;
import flixel.tweens.FlxTween;
import nape.phys.*;
import nape.shape.*;
import nape.geom.*;
import weapons.*;
import nape.callbacks.*;
import flixel.addons.nape.*;
import flixel.addons.display.*;

class PlayState extends FlxState
{
  public var player:Player;
  public var door:Door;

  override public function create():Void
  {
    super.create();

    FlxNapeSpace.init();
    // FlxNapeSpace.space.gravity = new Vec2(0, 600);
    FlxNapeSpace.space.worldLinearDrag = .25;
    FlxNapeSpace.space.worldAngularDrag = .25;
    // FlxNapeSpace.drawDebug = true;
    // FlxG.cameras.bgColor = 0xff0004a0;
    FlxG.camera.fade(FlxColor.BLACK, 0.5, true);
    // FlxG.fullscreen = true;
    // FlxG.scaleMode = new FillScaleMode();
    persistentDraw = false;

    var bg = new FlxBackdrop(AssetPaths.spacebg__jpg);
    add(bg);

    var drawings = new FlxGroup();
    add(drawings);

    var weaponSelector = new WeaponSelector();
    add(weaponSelector);

    var weapons = new FlxTypedGroup<weapons.Gun>();
    add(weapons);

    var world = {
      width: 4928,
      height: 3280,
    };
    FlxG.worldBounds.set(0, 0, world.width, world.height);
    FlxNapeSpace.createWalls(5, 5, world.width - 5, world.height - 5);
    player = new Player(world.width * .5, world.height * .5, weaponSelector);
    FlxG.camera.follow(player, FlxCameraFollowStyle.SCREEN_BY_SCREEN);
    // FlxG.camera.follow(player, FlxCameraFollowStyle.PLATFORMER);
    add(player);

    weapons.add(new LandSpray(drawings, 50));
    weapons.add(new Grenade(drawings, 5));
    weapons.add(new Bazooka(drawings, 5));
    weapons.forEach(function(gun) gun.setOwner(player.getPlayerData));
    weaponSelector.setWeapons(weapons);

    door = new Door(world.width * .75, world.height * .5, null, player);
    add(door);

    var collisionListener = new InteractionListener(
        CbEvent.BEGIN,
        InteractionType.COLLISION,
        CbType.ANY_BODY,
        CbType.ANY_BODY,
        onCollide
    );
    FlxNapeSpace.space.listeners.add(collisionListener);

  }
  private function onCollide(cb:InteractionCallback):Void
  {
      var body1 = cb.int1.castBody;
      var body2 = cb.int2.castBody;
      var type1 = body1.userData.type;
      var type2 = body2.userData.type;
      // explosion impulse
      if(type1 == "explosion" || type2 == "explosion") {
        var explosion = type1 == "explosion" ? body1 : body2;
        var other = type1 == "explosion" ? body2 : body1;
        var relative = other.position.sub(explosion.position);
        other.applyImpulse(relative);
      }
  }
  override public function update(elapsed):Void
  {
    super.update(elapsed);
    // FlxG.collide(drawings, walls, onCollide);
  }
  // private function onCollide(sprite1:FlxBasic, sprite2:FlxBasic)
  // {
  //   trace("collide sprites", sprite1, sprite2);
  // }
}
