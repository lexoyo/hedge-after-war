package terrain;

import nape.phys.*;
import flixel.util.*;
import flixel.*;
import flixel.math.*;
import flixel.addons.nape.*;
import nape.shape.*;
import nape.geom.*;
import flixel.util.FlxSpriteUtil;

class DestroyableSprite extends FlxNapeSprite
{
  // private var radius = 100;
  // var body = new Body();
  // private function circle(x:Float, y:Float) return y < 50 ? -1 : (Vec2.distance(Vec2.get(x, y), Vec2.get(radius/2, radius/2)) - radius/2);
  private var dots:Array<Array<Int>>;

  private var CELL_SIZE = 2;
  var lineStyle:LineStyle = { color: FlxColor.GREEN, thickness: 1 };
  var drawStyle:DrawStyle = { smoothing: true };
  public function new(x:Float = 0, y:Float = 0, dots:Array<Array<Int>>, bodyType:BodyType)
  {
    this.dots = dots;
    var TILE_SIZE = dots.length;

    // build body
    var bounds = new AABB(0, 0, TILE_SIZE - CELL_SIZE, TILE_SIZE - CELL_SIZE);
    var cellSize = Vec2.get(CELL_SIZE, CELL_SIZE);
    var quality:Int = 8;
    var geomPolyList = MarchingSquares.run(
        function (localX:Float, localY:Float) {
          return dots[Math.floor(localX)][Math.floor(localY)] == 0 ? 1 : -1;
        },
        bounds,
        cellSize,
        quality
    );
    // create the body
    var _body = new Body(bodyType);
    for(p in geomPolyList) {
      addGeomPoly(p, _body);
    }
    if(_body.shapes.length == 0) {
      trace("ERROR NOTHING IN THIS TILE", _body.shapes);
      throw("ERROR NOTHING IN THIS TILE");
    }
    super(x, y);
    // draw shape
    var vertices = [];
    var delta = 0; //TILE_SIZE/2;
    makeGraphic(TILE_SIZE, TILE_SIZE, FlxColor.TRANSPARENT, true);
    // body origin is 0, 0
    addPremadeBody(_body);
    origin.set(0, 0);
    if(mass == 0) {
      trace("ERROR NOTHING IN THIS TILE", _body.shapes);
      throw("ERROR NOTHING IN THIS TILE");
    }
    for(x in 0...TILE_SIZE) {
      for(y in 0...TILE_SIZE) {
        if(dots[x][y] != 0) {
          FlxSpriteUtil.drawCircle(this, x + delta, y + delta, 1, FlxColor.GREEN);
        }
      }
    }
  }
  override public function update(elapsed:Float):Void
  {
    super.update(elapsed);
  }

  // inspired by this code
  // https://github.com/DaVikingCode/Citrus-Engine/blob/master/src/citrus/objects/NapePhysicsObject.as#L156
  private function addGeomPoly(geomPoly:GeomPoly, _body:Body):Void {
    var polygon:Polygon = new Polygon(geomPoly); // <= here set the material
    var validation:ValidationResult = polygon.validity();

    if (validation == ValidationResult.VALID) {
      _body.shapes.add(polygon);
    }
    else if (validation == ValidationResult.CONCAVE) {
      var convex:GeomPolyList = geomPoly.convexDecomposition();
      convex.foreach(function(p:GeomPoly):Void {
        var poly = new Polygon(p);
        var valid:ValidationResult = poly.validity();
        if (valid == ValidationResult.VALID) {
          _body.shapes.add(poly);
        }
        else {
          trace("Invalid polygon/polyline", poly, valid);
          throw "Invalid polygon/polyline";
        }
      });
    } else {
      trace("Invalid polygon/polyline", polygon, validation);
      throw "Invalid polygon/polyline";
    }
  }
}
