package terrain;

import flixel.addons.nape.*;
import flixel.*;
import flixel.util.*;
import flixel.group.*;
import flixel.math.*;
import flixel.group.FlxGroup;
import flixel.FlxCamera.FlxCameraFollowStyle;
import flixel.tweens.FlxTween;
import flixel.addons.nape.*;
import flixel.addons.display.*;
import flixel.util.FlxSpriteUtil;
import haxe.ds.*;
import nape.shape.*;
import nape.phys.*;
import nape.geom.*;
import haxe.Timer;

class TerrainGenerator {
  public static function create(width:Int, height:Int):FlxGroup {

    // pixel
    trace("initDots");
    var dots = Timer.measure(function() return initDots(width, height));
    trace("createPath");
    Timer.measure(function() createPath(width, height, dots));
    trace("fillShapes");
    Timer.measure(function() fillShapes(width, height, dots));

    trace("dotsToNapeSprites");
    var group = Timer.measure(function() return dotsToNapeSpritesAsync(dots));


    return group;
  }
  public static var TILE_SIZE:Int = 50;




  private static function dotsToNapeSpritesAsync(dots:Array<Array<Int>>):FlxGroup {
    var numTilesH = Math.floor(dots.length/TILE_SIZE);
    var numTilesV = Math.floor(dots[0].length/TILE_SIZE);
    var tiles = new FlxGroup();
    var x = 0;
    var y = 0;
    function loop() {
      var tile = createTile(x, y, dots);
      if(tile != null) {
        if(tile.mass != 0) tiles.add(tile);
        else trace("Mass is 0 ?!");
      }
      if(++x<numTilesH) {
        // Timer.delay(loop, 0);
        loop();
      }
      else if(++y<numTilesV) {
        x = 0;
        Timer.delay(loop, 0);
        // loop();
      }
    }
    loop();
    return tiles;
  }
  private static function dotsToNapeSprites(dots:Array<Array<Int>>):FlxGroup {
    var numTilesH = Math.floor(dots.length/TILE_SIZE);
    var numTilesV = Math.floor(dots[0].length/TILE_SIZE);
    var tiles = new FlxGroup();

    // debug
    // var drawing = new FlxSprite();
    // tiles.add(drawing);
    // drawing.makeGraphic(dots.length, dots[0].length);
    // trace("drawDots");
    // Timer.measure(function() drawDots(dots.length, dots[0].length, dots, drawing));

    // group dots by tile
    for(x in 0...numTilesH) {
      for(y in 0...numTilesV) {
        var tile = createTile(x, y, dots);
        if(tile != null) {
          if(tile.mass != 0) tiles.add(tile);
          else trace("Mass is 0 ?!");
          // FlxSpriteUtil.drawCircle(drawing, x * TILE_SIZE, y * TILE_SIZE, 3, FlxColor.ORANGE);
        }
      }
    }
    return tiles;
  }
  private static function createTile(x:Int, y:Int, dots:Array<Array<Int>>):Null<FlxSprite> {
    var globalX = x * TILE_SIZE;
    var globalY = y * TILE_SIZE;
    var tileDots:Array<Array<Int>> = [];
    var hasDots = false;
    for(relX in 0...TILE_SIZE) {
      tileDots[relX] = [];
      for(relY in 0...TILE_SIZE) {
        var dot = dots[globalX + relX][globalY + relY];
        tileDots[relX][relY] = dot;
        hasDots = dot != 0 ? true : hasDots;
      }
    }
    try {
      if(hasDots) return new DestroyableSprite(globalX, globalY, tileDots,
        // BodyType.DYNAMIC);
        BodyType.STATIC);
    } catch(e:Dynamic) {
      trace("Error catched and tile removed", e);
    }
    trace("Empty tile removed");
    return null;
  }
  private static function drawDots(width:Int, height:Int, dots:Array<Array<Int>>, drawing:FlxSprite) {
    // FlxSpriteUtil.fill(drawing, FlxColor.BLACK);
    for(x in 0...width) {
      for(y in 0...height) {
        if(dots[x][y] != 0) FlxSpriteUtil.drawCircle(
          drawing,
          x,
          y,
          1,
          switch(dots[x][y]) {
            case 1: FlxColor.ORANGE;
            case 2: FlxColor.BLUE;
            default: FlxColor.PINK;
          });
      }
    }
  }
  private static function initDots(width:Int, height:Int):Array<Array<Int>> {
    var dots:Array<Array<Int>> = [];
    for(x in 0...width) {
      dots[x] = [];
      for(y in 0...height) {
        dots[x][y] = 0;
      }
    }
    return dots;
  }
  private static function fillShapes(width:Int, height:Int, dots:Array<Array<Int>>):Void {

    for(x in 0...width) {
      var fillMode = false;
      for(y in 0...height-1) {
        if(dots[x][y] != 0 && dots[x][y+1] == 0)
          fillMode = !fillMode;
        else if(fillMode) dots[x][y] = 2;
      }
    }
  }
  private static function walk(width:Int, height:Int, onUpdate:Int->Int->Void):Void {
    var x = 0.0;
    var y = rand(height / 2, [-height/3, height/3]);
    var velocity = {
      x: 0.0,
      y: 0.0,
    };
    while(x < width) {
      velocity.x = rand(velocity.x, [-10, 10], [0, 10]);
      velocity.y = rand(velocity.y, [-10, 12], [-10, 10]);
      x += velocity.x;
      x = rand(x, null, [0, width]);
      y += velocity.y;
      y = rand(y, null, [0, height]);
      onUpdate(Math.round(x), Math.round(y));
    }
  }
  private static function createPath(width:Int, height:Int, dots:Array<Array<Int>>):Void {
    var oldX = -1;
    var oldY = -1;
    walk(width, height, function(x, y) {
      if(oldX < 0) oldX = x ;
      if(oldY < 0) oldY = y ;

      // var dotsAtIdx = dots.get(Math.floor(x));
      // if(dotsAtIdx == null) dotsAtIdx = new List();
      // dots.set(Math.floor(x), dotsAtIdx);
      // dotsAtIdx.add(Math.floor(y));

      // interpolate Y
      var left:Int = Math.round(Math.min(oldX, x));
      var right:Int = Math.round(Math.max(oldX, x));
      for(idx in left...right) {
        var completionRate = right == left ? 1 : (idx - left) / (right - left);
        if(oldX >= x) completionRate = 1 - completionRate;
        var interpolatedY = Math.round((y * completionRate + oldY * (1-completionRate)));
        dots[idx][interpolatedY] = 1;
      }
      // interpolate X
      var top:Int = Math.round(Math.min(oldY, y));
      var bottom:Int = Math.round(Math.max(oldY, y));
      for(idy in top...bottom) {
        var completionRate = top == bottom ? 1 : (idy - top) / (bottom - top);
        if(oldY >= y) completionRate = 1 - completionRate;
        var interpolatedX = Math.round((x * completionRate + oldX * (1-completionRate)));
        if(interpolatedX >= 0 && interpolatedX < width)
          dots[interpolatedX][idy] = 1;
      }
      oldX = x;
      oldY = y;
    });
  }

  static function rand(value:Float, rndRange:Array<Float> = null, range:Array<Float> = null):Float {
    var res:Float = value;
    if(rndRange != null) {
      res += Math.random() * (rndRange[1] - rndRange[0]) + rndRange[0];
    }
    if(range != null) {
      res = Math.min(res, range[1]);
      res = Math.max(res, range[0]);
    }
    return res;
  }
}
