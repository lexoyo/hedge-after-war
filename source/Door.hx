package;

import nape.phys.*;
import flixel.util.*;
import flixel.*;
import flixel.math.*;
import flixel.addons.nape.*;

class Door extends FlxNapeSprite
{
  // private static var SIZE_X = 10;
  // private static var SIZE_Y = 20;
  private var target:Player;
  private var playState:Class<FlxState>;
  public function new(x:Float, y:Float, playState:Class<FlxState>, target:Player)
  {
    super(x, y);
    this.playState = playState;
    this.target = target;
    // makeGraphic(Math.ceil(SIZE_X), Math.ceil(SIZE_Y), FlxColor.GREEN);
    loadGraphic(AssetPaths.Timebox__png, true, 50, 81);
    animation.add("Ready", [0]);
    animation.play("Ready");
    createRectangularBody(0, 0, BodyType.KINEMATIC);
    updateHitbox();
  }
  override public function update(elapsed:Float):Void
  {
    super.update(elapsed);
    // door collision
    for(sprite in FlxNapeSpace.space.bodiesInBody(body)) {
      if(sprite == target.body) {
        FlxG.switchState(Type.createInstance(playState, []));
      }
    }
  }
}

