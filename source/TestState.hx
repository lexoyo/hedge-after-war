package;

import flixel.FlxState;
import flixel.addons.nape.*;
import flixel.*;
import flixel.util.*;
import flixel.group.*;
import flixel.math.*;
import flixel.group.FlxGroup;
import flixel.FlxCamera.FlxCameraFollowStyle;
import flixel.tweens.FlxTween;
import flixel.addons.nape.*;
import flixel.addons.display.*;
import haxe.ds.*;
import weapons.*;
import terrain.*;
import nape.geom.*;

class TestState extends FlxState
{
  var world = {
    width: FlxG.width,
    height: FlxG.height,
  };
  var gun:LandSpray;
  override public function create():Void
  {
    super.create();

    FlxNapeSpace.init();
    FlxNapeSpace.space.gravity = new Vec2(0, 600);
    FlxG.autoPause = false;
    // FlxG.worldBounds.set(0, 0, world.width, world.height);
    // FlxG.camera.follow(walker, FlxCameraFollowStyle.SCREEN_BY_SCREEN);
    // FlxG.camera.follow(player, FlxCameraFollowStyle.PLATFORMER);
    FlxNapeSpace.createWalls(0, 0, Math.floor(world.width / TerrainGenerator.TILE_SIZE)*TerrainGenerator.TILE_SIZE, Math.floor(world.height / TerrainGenerator.TILE_SIZE)*TerrainGenerator.TILE_SIZE);

    var terrain = TerrainGenerator.create(world.width, world.height);
    add(terrain);

    var drawings = new FlxGroup();
    add(drawings);

    gun = new LandSpray(drawings, 500);
    gun.setOwner(function() {
      #if !FLX_NO_MOUSE
        var mouseX = FlxG.mouse.x;
        var mouseY = FlxG.mouse.y;
      #else
        var mouseX = FlxG.touches.getFirst().justPressedPosition.x;
        var mouseY = FlxG.touches.getFirst().justPressedPosition.y;
      #end
      var mouseAngle = Math.atan2(mouseY, mouseX);
      return {
        x: 100.0,
        y: 100.0,
        width: 10.0,
        height: 10.0,
        angle: 0.0,
        speedX: 0.0,
        speedY: 0.0,
        mouseX: mouseX,
        mouseY: mouseY,
        mouseAngle: mouseAngle,
      };
    });
    add(gun);
  }
  override public function update(elapsed):Void
  {
    super.update(elapsed);
    #if !FLX_NO_MOUSE
    if(FlxG.mouse.justPressed) for(i in 0...100)
    {
      var bullet = new weapons.Bullet({
        boost: 0,
        size: 4,
        parentX: 4*i + FlxG.mouse.getPosition().x,
        parentY: 10,
        angle: 0,
        parentSpeedX: 0,
        parentSpeedY: 0,
      }, null);
      add(bullet);
      gun.startFire(function(bullet, energy) {});
    }
    if(FlxG.mouse.justReleased)
    {
      gun.stopFire();
    }

    #end
    // FlxG.collide(drawings, walls, onCollide);
  }
  // private function onCollide(sprite1:FlxBasic, sprite2:FlxBasic)
  // {
  //   trace("collide sprites", sprite1, sprite2);
  // }
}
