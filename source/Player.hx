package;

import flixel.addons.nape.*;
import flixel.*;
import flixel.group.*;
import flixel.group.FlxGroup;
import weapons.*;
import PlayerData;
import flixel.input.keyboard.*;

class Player extends FlxNapeSprite
{
  private static var TILE_SIZE = 32;
  private static var SIZE = 10;
  private var weaponsSelector:WeaponSelector;
  public function new(x:Float, y:Float, weaponsSelector:WeaponSelector) {
    super(x, y);
    loadGraphic(AssetPaths.Hedgehog__png, true, TILE_SIZE, TILE_SIZE);
    animation.add("Flying", [15]);
    animation.add("Happy", [16]);
    animation.add("Affraid", [17]);
    animation.add("Yelling", [19]);
    animation.play("Flying");
    createCircularBody();
    this.weaponsSelector = weaponsSelector;
    body.userData.type = 'player';
  }
  private function onFire(bullet: Bullet, energy:Float) {
    body.velocity.x += bullet.mass * energy * Math.cos(bullet.body.rotation + Math.PI);
    body.velocity.y += bullet.mass * energy * Math.sin(bullet.body.rotation + Math.PI);
    body.rotation = bullet.body.rotation;
  }
  public function getPlayerData():PlayerData {
    #if !FLX_NO_MOUSE
      var mouseX = FlxG.mouse.x;
      var mouseY = FlxG.mouse.y;
    #else
      var mouseX = FlxG.touches.getFirst().justPressedPosition.x;
      var mouseY = FlxG.touches.getFirst().justPressedPosition.y;
    #end
    mouseX -=  Math.round(x);
    mouseY -=  Math.round(y);
    var mouseAngle = Math.atan2(mouseY, mouseX);
    return {
      x: x,
      y: y,
      width: width,
      height: height,
      angle: angle,
      speedX: body.velocity.x,
      speedY: body.velocity.y,
      mouseX: mouseX,
      mouseY: mouseY,
      mouseAngle: mouseAngle,
    };
  }
  override public function update(elapsed):Void
  {
    #if !FLX_NO_MOUSE
      var justPressed:Bool = FlxG.mouse.justPressed;
      var justReleased:Bool = FlxG.mouse.justReleased;
    #else
      var justPressed:Bool = FlxG.touches.justStarted().length > 0;
      var justReleased:Bool = FlxG.touches.justReleased().length > 0;
    #end
    if (justPressed && weaponsSelector.focus == false)
    {
      weaponsSelector.selected.startFire(onFire);
    }
    else if (justReleased && weaponsSelector.focus == false)
    {
      weaponsSelector.selected.stopFire();
    }
    super.update(elapsed);
  }
}
