package;

import flixel.FlxState;
import flixel.addons.nape.*;
import flixel.*;
import flixel.util.*;
import flixel.group.*;
import flixel.FlxCamera.FlxCameraFollowStyle;
import flixel.tweens.FlxTween;
import nape.phys.*;
import nape.geom.*;

class TerrainState extends FlxState
{
  override public function create():Void
  {
    super.create();
    var world = {
      width: 600,
      height: 400,
    };
    FlxNapeSpace.init();
    // FlxG.worldBounds.set(0, 0, world.width, world.height);
    FlxNapeSpace.createWalls(0, 0, world.width, world.height);
    FlxNapeSpace.space.gravity = new Vec2(0, 600);
    FlxNapeSpace.space.worldLinearDrag = .25;
    FlxNapeSpace.space.worldAngularDrag = .25;
    persistentDraw = false;
    FlxG.camera.fade(FlxColor.BLACK, 0.5, true);
    add(new Terrain(300, 150));
    // add(new Terrain(100, 150));
    // add(new Terrain(400, 150));
    FlxG.camera.follow(cast members[0], FlxCameraFollowStyle.SCREEN_BY_SCREEN);

  }
  override public function update(elapsed):Void
  {
    super.update(elapsed);
    #if !FLX_NO_MOUSE
    if(FlxG.mouse.justPressed) for(i in 0...100)
    {
      var bullet = new weapons.Bullet({
        boost: 0,
        size: 4,
        parentX: 4*i,
        parentY: 10,
        angle: 0,
        parentSpeedX: 0,
        parentSpeedY: 0,
      }, null);
      add(bullet);
    }
    #end
  }
}
