package weapons;

import flixel.util.*;
import flixel.*;
import flixel.group.*;


class Gun extends FlxGroup
{
  private static var FULL_POWER_TIME:Float = 100;
  private static var FULL_POWER_SIZE:Int = 100;
  private var drawings:FlxGroup;
  private var mouseDown:Bool = false;
  private var getPlayerData: Void->PlayerData;

  private var fireCbk:Bullet->Float->Void;
  public function new(drawings:FlxGroup, maxSize:Int = 100)
  {
    this.drawings = drawings;
    super(maxSize);
  }
  public function setOwner(getPlayerData:Void->PlayerData)
  {
    this.getPlayerData = getPlayerData;
  }
  public function getOwnerPhysics()
  {
    var owner = getPlayerData();
    return {
      x: owner.x+(owner.width/2),
      y: owner.y+(owner.height/2),
      angle: owner.angle,
      speedX: owner.speedX,
      speedY: owner.speedY,
      mouseX: owner.mouseX,
      mouseY: owner.mouseY,
      mouseAngle: owner.mouseAngle,
    }
  }
  public function getName():String
  {
    return "Gun";
  }
  public function startFire(fireCbk:Bullet->Float->Void):Void
  {
    this.fireCbk = fireCbk;
    mouseDown = true;
  }
  public function stopFire():Void
  {
    mouseDown = false;
  }
  public function onExplode(bullet: Bullet):Void
  {}
  private function continuousFire():Void
  {}
  override public function update(elapsed:Float):Void {
    super.update(elapsed);
    forEachDead(function(bullet) {
      remove(bullet);
    });
    if(mouseDown) continuousFire();
  }
}
