package weapons;

import flixel.util.*;
import flixel.*;
import flixel.group.*;
import nape.shape.*;

class Bazooka extends Gun
{
  private var power:Float = 0;
  private var BULLET_IMPULSE:Float = 500;
  private var PLAYER_IMPULSE:Float = 500;
  private var sprite:FlxSprite;
  public function new(drawings:FlxGroup, maxSize:Int = 100)
  {
    super(drawings, maxSize);
  }
  override public function getName():String
  {
    return "Bazooka";
  }
  override public function startFire(fireCbk:Bullet->Float->Void):Void
  {
    super.startFire(fireCbk);
    power = 0;
    sprite = new FlxSprite(-10000);
    sprite.makeGraphic(100, 10, FlxColor.RED, true);
    redrawSprite();
    haxe.Timer.delay(function() {
      drawings.add(sprite);
    }, 10);
  }
  private function redrawSprite():Void
  {
    var w = Math.ceil(Gun.FULL_POWER_SIZE * power / Gun.FULL_POWER_TIME);
    var ownerPhysics = getOwnerPhysics();
    sprite.origin.x = 0;
    sprite.origin.y = 5;
    sprite.setGraphicSize(w);
    sprite.x = ownerPhysics.x;
    sprite.y = ownerPhysics.y - 5;
    sprite.angle = ownerPhysics.mouseAngle * 180 / Math.PI;
  }
  override public function stopFire():Void
  {
    super.stopFire();
      var ownerPhysics = getOwnerPhysics();
      var bulletImpulse = BULLET_IMPULSE * power / Gun.FULL_POWER_TIME;
      var playerImpulse = PLAYER_IMPULSE * power / Gun.FULL_POWER_TIME;
      var bullet = new Bullet({
      boost: bulletImpulse,
      size: 32,
      maxImpulse: 0,
      parentX: ownerPhysics.x,
      parentY: ownerPhysics.y,
      angle: ownerPhysics.mouseAngle,
      parentSpeedX: ownerPhysics.speedX,
      parentSpeedY: ownerPhysics.speedY,
      image: AssetPaths.BazookaShell__png,
    }, onExplode);
    add(bullet);
    drawings.remove(sprite);
    if(fireCbk != null) fireCbk(bullet, playerImpulse);
  }
  override public function continuousFire():Void
  {
    power++;
    power = Math.min(power, Gun.FULL_POWER_TIME);
    if(sprite != null) {
      redrawSprite();
    }
  }
  override public function onExplode(bullet: Bullet):Void
  {
    super.onExplode(bullet);
    var explosion = new Explosion(bullet.x, bullet.y, 100);
    add(explosion);
  }
}
