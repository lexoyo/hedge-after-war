package weapons;

import flixel.util.*;
import flixel.*;
import flixel.math.*;
import flixel.addons.nape.*;
import flixel.system.FlxAssets;

typedef BulletOptions = {
  boost:Float,
  size:Float,
  ?life:Null<Float>,
  ?maxImpulse:Null<Float>,
  angle:Float,
  ?maxRandAngle:Null<Float>, // maximum value of a random number multiplied with the bullet angle
  ?angularVelocity:Null<Float>,
  parentX:Float,
  parentY:Float,
  parentSpeedX:Float,
  parentSpeedY:Float,
  ?image:Null<FlxGraphicAsset>,
};

class Bullet extends FlxNapeSprite
{
  private static var INITIAL_DISTANCE = 10;
  private var options: BulletOptions;
  private var onExplode: Bullet->Void;
  public function new(options: BulletOptions, onExplode: Bullet->Void)
  {
    this.options = options;
    this.onExplode = onExplode;
    var radius = options.size;
    var halfRadius = options.size / 2;
    var randAngle = options.angle;
    if(options.maxRandAngle != null) randAngle += options.maxRandAngle * 2 * (Math.random() - .5);
    super(
      options.parentX + INITIAL_DISTANCE * Math.cos(randAngle),
      options.parentY + INITIAL_DISTANCE * Math.sin(randAngle),
      options.image, false
    );
    if(options.angularVelocity != null)
    {
      angularVelocity = options.angularVelocity;
    }
    if(options.image == null)
    {
      makeGraphic(
        Math.ceil(radius),
        Math.ceil(radius),
        FlxColor.TRANSPARENT);
      FlxSpriteUtil.drawCircle(
        this,
        halfRadius,
        halfRadius,
        halfRadius,
        FlxColor.RED);
      createCircularBody(radius);
    }
    else {
      // scale = new flixel.math.FlxPoint(options.size / width, options.size / width);
      // updateHitbox();
      createCircularBody();
    }
    body.userData.type = 'bullet';
    body.velocity.x = options.parentSpeedX + options.boost * Math.cos(randAngle);
    body.velocity.y = options.parentSpeedY + options.boost * Math.sin(randAngle);
    body.rotation = randAngle;
  }
  override public function update(elapsed:Float):Void
  {
    super.update(elapsed);
    // trace("crush", body.crushFactor());
    if(options.maxImpulse != null && body.tangentImpulse().length > options.maxImpulse) {
      kill();
    }
    if(options.life != null && options.life-- <= 0) {
      kill();
    }
  }
  override public function kill():Void
  {
    onExplode(this);
    super.kill();
  }
}
