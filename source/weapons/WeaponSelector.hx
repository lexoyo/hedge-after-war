package weapons;

import flixel.*;
import flixel.ui.*;
import flixel.util.*;
import flixel.group.*;
import flixel.group.FlxGroup;
import flixel.input.mouse.*;

class WeaponSelector extends FlxGroup
{
  private var weapons = new FlxTypedGroup<Gun>();
  public var selected: Gun;
  public var focus = false;
  private var selectionMarker: FlxSprite;
  private static var MARGIN = 5;
  private static var TILE_SIZE = 32;
  public function new(maxSize:Int = 0)
  {
    super(maxSize);
    FlxMouseEventManager.init();
    selectionMarker = new FlxSprite(-100, MARGIN - 1);
    selectionMarker.makeGraphic(TILE_SIZE + 2, TILE_SIZE + 2, FlxColor.WHITE);
    selectionMarker.alpha = .8;
  }
  public function setWeapons(weapons:FlxTypedGroup<Gun>):Void {
    this.weapons = weapons;
    selected = weapons.getFirstAlive();
    selectionMarker.scrollFactor.x = 0;
    selectionMarker.scrollFactor.y = 0;
    updateUi();
  }
  public function updateUi():Void
  {
    FlxMouseEventManager.removeAll();
    clear();
    add(selectionMarker);
    var prevWidth:Float = MARGIN;
    weapons.forEachAlive(function(gun:Gun)
    {
      // var btn = new FlxSpriteButton(prevWidth, 0);
      // btn.createTextLabel(gun.getDisplayName());
      var btn = new FlxSprite(prevWidth, MARGIN);
      btn.loadGraphic(AssetPaths.Ammos__png, true, TILE_SIZE, TILE_SIZE);
      btn.animation.add("Grenade", [0]);
      btn.animation.add("Bazooka", [8]);
      btn.animation.add("Land Spray", [19]);
      btn.animation.add("Grenade", [0]);
      btn.animation.play(gun.getName());
      btn.scrollFactor.x = 0;
      btn.scrollFactor.y = 0;
      if(selected == gun) {
        selectionMarker.x = prevWidth - 1;
      }
      prevWidth += btn.width + MARGIN;
      add(btn);
      FlxMouseEventManager.add(btn,
        function(btn) {
          focus = true;
        }, function(btn) {
          haxe.Timer.delay(function() focus = false, 10);
          select(gun);
        });
    });
  }
  public function select(gun:Gun):Void {
    selected = gun;
    updateUi();
  }
}
