package weapons;

import flixel.util.*;
import flixel.*;
import flixel.group.*;

class LandSpray extends Gun
{
  private var PLAYER_IMPULSE = 10;
  private var BULLET_IMPULSE = 100;
  public function new(drawings:FlxGroup, maxSize:Int = 100)
  {
    super(drawings, maxSize);
  }
  override public function getName():String
  {
    return "Land Spray";
  }
  override private function continuousFire():Void
  {
    super.continuousFire();
    var ownerPhysics = getOwnerPhysics();
    var bullet = new Bullet({
      boost: BULLET_IMPULSE,
      size: 2,
      life: 100,
      parentX: ownerPhysics.x,
      parentY: ownerPhysics.y,
      angle: ownerPhysics.mouseAngle,
      maxRandAngle: .05,
      parentSpeedX: ownerPhysics.speedX,
      parentSpeedY: ownerPhysics.speedY,
    }, onExplode);
    add(bullet);
    if(fireCbk != null) fireCbk(bullet, PLAYER_IMPULSE);
  }
  override public function update(elapsed:Float):Void {
    super.update(elapsed);
    forEachDead(function(bullet) {
      remove(bullet);
    });
    if(mouseDown) continuousFire();
  }
}
