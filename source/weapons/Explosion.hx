package weapons;

import flixel.util.*;
import flixel.*;
import flixel.math.*;
import nape.phys.*;
import nape.shape.*;
import flixel.addons.nape.*;

class Explosion extends FlxNapeSprite
{
  private var blast: Float;
  private var life: Float = 2;
  public function new(x: Float, y: Float, blast:Float)
  {
    super(x, y);
    this.blast = blast;
    var radius = Math.ceil(blast / 2);
    makeGraphic(
      Math.ceil(blast),
      Math.ceil(blast),
      FlxColor.TRANSPARENT);
    FlxSpriteUtil.drawCircle(
      this,
      radius,
      radius,
      radius,
      FlxColor.RED);
    createCircularBody(radius);
    // do not change explosion position when it is colliding
    body.allowMovement = false;
    body.allowRotation = false;
    body.inertia = 1000;
    body.userData.type = 'explosion';
  }
  override public function update(elapsed:Float):Void
  {
    super.update(elapsed);
    if(--life <= 0) {
      kill();
    }
  }
}
