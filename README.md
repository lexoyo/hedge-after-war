
Hedge wars spin off

A hedgehog is lost in space, with zero gravity. When it shoots with a weapon it makes him move and also kills enemies and. digs the terrain.

Things to use from the hedgewars:

* Use hedge wars sounds and graphics. No text, only hedge wars voices (localized).
* Weapons
* Destructible terrain
* Sticky mines follow him


Gameplay ideas

* In an asteroid's tunnels, must escape
* With the weapons destroy mines and terrain to go to a black whole (or time capsule?)
* Make a tunnel to go to a black whole
* Pass through a series of freezers beams (be quick to avoid freezing, hide behind moving rocks)
* Kill or rescue an angry hedgehog shooting randomly in any direction and trapped in a time capsule



Installation

```
$ sudo dnf install haxe
$ haxelib setup
$ haxelib install flixel
$ haxelib run lime setup
$ haxelib install flixel-tools
$ haxelib run flixel-tools setup
$ haxelib install flixel-addons
$ haxelib install nape
$ haxelib install hscript
```

Build and serve
```
$ lime build -debug html5
$ http-server export/html5/bin/
```
